package com.ecom.inventoryservice.entity;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Entity
@Table(name = "inventory")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Inventory {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "inventoryID")
    private Long inventoryID;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "qty")
    private Integer qty;

    @Column(name = "price")
    private Double price;

    @Column(name = "location")
    private String location;

    @Column(name = "type")
    private String type;

    @Column(name = "tagsize")
    private String tagSize;

    @Column(name = "length")
    private Double length;

    @Column(name = "width")
    private Double width;

    @Column(name = "color")
    private String color;

    @Column(name = "era")
    private String era;

    @Column(name = "brand")
    private String brand;

    @Column(name = "material")
    private String material;
    public static class InventoryBuilder {
        private Long inventoryID;
        private String title;
        private String description;
        private Integer qty;
        private Double price;
        private String location;
        private String type;
        private String tagSize;
        private Double length;
        private Double width;
        private String color;
        private String era;
        private String brand;
        private String material;

        public InventoryBuilder inventoryID(Long inventoryID) {
            this.inventoryID = inventoryID;
            return this;
        }

        public InventoryBuilder title(String title) {
            this.title = title;
            return this;
        }

        public InventoryBuilder description(String description) {
            this.description = description;
            return this;
        }

        public InventoryBuilder qty(Integer qty) {
            this.qty = qty;
            return this;
        }

        public InventoryBuilder price(Double price) {
            this.price = price;
            return this;
        }

        public InventoryBuilder location(String location) {
            this.location = location;
            return this;
        }

        public InventoryBuilder type(String type) {
            this.type = type;
            return this;
        }

        public InventoryBuilder tagSize(String tagSize) {
            this.tagSize = tagSize;
            return this;
        }

        public InventoryBuilder length(Double length) {
            this.length = length;
            return this;
        }

        public InventoryBuilder width(Double width) {
            this.width = width;
            return this;
        }

        public InventoryBuilder color(String color) {
            this.color = color;
            return this;
        }

        public InventoryBuilder era(String era) {
            this.era = era;
            return this;
        }

        public InventoryBuilder brand(String brand) {
            this.brand = brand;
            return this;
        }

        public InventoryBuilder material(String material) {
            this.material = material;
            return this;
        }

        public Inventory build() {
            return new Inventory(inventoryID, title, description, qty, price, location, type, tagSize, length, width, color, era, brand, material);
        }
    }
}