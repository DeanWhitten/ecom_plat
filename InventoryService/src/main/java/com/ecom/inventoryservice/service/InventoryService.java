package com.ecom.inventoryservice.service;

import com.ecom.inventoryservice.entity.Inventory;
import com.ecom.inventoryservice.repository.InventoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class InventoryService {
    @Autowired
    private InventoryRepository inventoryRepository;

    public Inventory createProduct(Inventory product) {
        return inventoryRepository.save(product);
    }

    public List<Inventory> getAllProducts() {
        return inventoryRepository.findAll();
    }

    public Optional<Inventory> getProductById(Long id) {
        return inventoryRepository.findById(id);
    }

    public Inventory updateProduct(Long id, Inventory product) {
        Optional<Inventory> optionalProduct = inventoryRepository.findById(id);
        if (optionalProduct.isPresent()) {
            Inventory existingProduct = optionalProduct.get();
            existingProduct.setTitle(product.getTitle());
            existingProduct.setDescription(product.getDescription());
            existingProduct.setQty(product.getQty());
            existingProduct.setPrice(product.getPrice());
            existingProduct.setLocation(product.getLocation());
            existingProduct.setType(product.getType());
            existingProduct.setTagSize(product.getTagSize());
            existingProduct.setLength(product.getLength());
            existingProduct.setWidth(product.getWidth());
            existingProduct.setColor(product.getColor());
            existingProduct.setEra(product.getEra());
            existingProduct.setBrand(product.getBrand());
            existingProduct.setMaterial(product.getMaterial());

            return inventoryRepository.save(existingProduct);
        } else {
            return null;
        }
    }

    public boolean deleteProduct(Long id) {
        Optional<Inventory> optionalProduct = inventoryRepository.findById(id);

        if (optionalProduct.isPresent()) {
            inventoryRepository.deleteById(id);
            return true;
        } else {
            return false;
        }
    }
}
