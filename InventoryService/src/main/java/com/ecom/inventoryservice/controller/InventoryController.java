package com.ecom.inventoryservice.controller;
import com.ecom.inventoryservice.entity.Inventory;
import com.ecom.inventoryservice.service.InventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/products")
public class InventoryController {
    @Autowired
    private InventoryService inventoryService;

    @PostMapping
    public ResponseEntity<Inventory> createProduct(@RequestParam("title") String title,
                                                   @RequestParam("description") String description,
                                                   @RequestParam("qty") Integer qty,
                                                   @RequestParam("price") Double price,
                                                   @RequestParam("location") String location,
                                                   @RequestParam("type") String type,
                                                   @RequestParam("tagSize") String tagSize,
                                                   @RequestParam("length") Double length,
                                                   @RequestParam("width") Double width,
                                                   @RequestParam("color") String color,
                                                   @RequestParam("era") String era,
                                                   @RequestParam("brand") String brand,
                                                   @RequestParam("material") String material) {
        Inventory inventory = Inventory.builder()
                .title(title)
                .description(description)
                .qty(qty)
                .price(price)
                .location(location)
                .type(type)
                .tagSize(tagSize)
                .length(length)
                .width(width)
                .color(color)
                .era(era)
                .brand(brand)
                .material(material)
                .build();

        Inventory savedProduct = inventoryService.createProduct(inventory);
        return ResponseEntity.status(HttpStatus.CREATED).body(savedProduct);
    }

    @GetMapping
    public List<Inventory> getAllProducts() {
        return inventoryService.getAllProducts();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Inventory> getProductById(@PathVariable Long id) {
        Optional<Inventory> product = inventoryService.getProductById(id);
        return product.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PutMapping("/{id}")
    public ResponseEntity<Inventory> updateProduct(@PathVariable Long id,
                                                   @RequestParam("title") String title,
                                                   @RequestParam("description") String description,
                                                   @RequestParam("qty") Integer qty,
                                                   @RequestParam("price") Double price,
                                                   @RequestParam("location") String location,
                                                   @RequestParam("type") String type,
                                                   @RequestParam("tagSize") String tagSize,
                                                   @RequestParam("length") Double length,
                                                   @RequestParam("width") Double width,
                                                   @RequestParam("color") String color,
                                                   @RequestParam("era") String era,
                                                   @RequestParam("brand") String brand,
                                                   @RequestParam("material") String material) {
        Optional<Inventory> productToUpdate = inventoryService.getProductById(id);
        if (!productToUpdate.isPresent()) {
            return ResponseEntity.notFound().build();
        }

        Inventory updatedProduct = productToUpdate.get();
        updatedProduct.setTitle(title);
        updatedProduct.setDescription(description);
        updatedProduct.setQty(qty);
        updatedProduct.setPrice(price);
        updatedProduct.setLocation(location);
        updatedProduct.setType(type);
        updatedProduct.setTagSize(tagSize);
        updatedProduct.setLength(length);
        updatedProduct.setWidth(width);
        updatedProduct.setColor(color);
        updatedProduct.setEra(era);
        updatedProduct.setBrand(brand);
        updatedProduct.setMaterial(material);

        Inventory savedProduct = inventoryService.updateProduct(id, updatedProduct);
        return ResponseEntity.ok(savedProduct);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteProduct(@PathVariable Long id) {
        boolean deleted = inventoryService.deleteProduct(id);
        if (deleted) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
