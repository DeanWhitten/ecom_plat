
CREATE SEQUENCE hibernate_sequence START 1;

CREATE TABLE hibernate_sequence (
    next_val BIGINT
);

INSERT INTO hibernate_sequence VALUES (1);

CREATE TABLE Inventory (
                           InventoryID BIGINT DEFAULT nextval('hibernate_sequence') PRIMARY KEY,
                           Title varchar(255),
                           Description varchar(255),
                           Qty int,
                           Price decimal(10,2),
                           Location varchar(255),
                           Type varchar(255),
                           TagSize varchar(255),
                           Length decimal(10,2),
                           Width decimal(10,2),
                           Color varchar(255),
                           Era varchar(255),
                           Brand varchar(255),
                           Material varchar(255)
);